#!/bin/bash
if [[ $(id -u) -gt 0 ]]
then
    echo "run this command as root or sudo otherwise this script might fail"
    exit
fi

if [ "$#" -ne 1 ]; then

  while true; do
    read -p "Enter full path of your SIP installation without trailing slash: " SIPDIR

        if [[ $SIPDIR = "q" ]] || [[ $SIPDIR = "Q" ]] 
        then 
            exit 999
        fi
        if test -f "$SIPDIR/sip.py"; then
            echo "Installing plugin in $SIPDIR"
            break
        fi
        echo "$SIPDIR not found, please try again"
  done

  else
    SIPDIR="$1"
fi

cp -u pcf857x.html $SIPDIR/templates/pcf857x.html
cp -u pcf857x.py $SIPDIR/plugins/pcf857x.py
cp -u pcf857x.manifest $SIPDIR/plugins/manifests
chmod +x $SIPDIR/plugins/pcf857x.py
echo 'Installation of plugin done'
