# pcf857x_plugin_sip

V0.4.0

Plugin for IO extender modules based on Philips/NXP PCF875x  I2C chips.

With this plugin you can have 64 stations on the [SIP](https://dan-in-ca.github.io/SIP/) System.

### (beta)testers and feedback needed
The plugin and the docs are in beta and therefore not yet available in the SIP_Plugins repository. A manual install has to be done. If you are nog familiar with installing the files feel free to contact the author on Gitlab or the SIP [forum](https://nosack.com/sipforum/index.php).

### Install
Pick a directory outside your SIP installation and run
- `git clone https://gitlab.com/seventer/pcf857x_plugin_sip.git`

- `cd pcf857x_plugin_sip`

- Run as root or sudo `./install.sh` 

If the installation is finished start SIP and configure the plugin as mentioned in the docs.

<img src="static/plugin_setup1.png" width="80%"/>
