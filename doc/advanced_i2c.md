# Advanced i2c
From within the plugin you can directly control the IO pins from a extender board. This can help you for identifying the boards, relays, stations etc.
Commands are send to a specific board by the address you have previously configured in the plugin setup. To set a pin/station you have to set all ports at once by sending data to the board.
The data is in hexadecimal format and is inverted. So to set all pins off you would send 0xFF. Alternatively, to set all pins/stations on you have to send 0x00.

With 8 pins/channels there are 256 possibilities. The following table shows the first 16 possibilities (pin 1..4). To addres the other pins, use the same table and add the hex value. 

|8|7|6|5| |4|3|2|1|data|data to send|
|-|-|-|-|-|-|-|-|-|----|-------------|
|0|0|0|0| |0|0|0|0|0x00|0xFF|
|0|0|0|0| |0|0|0|1|0x01|0xFE|
|0|0|0|0| |0|0|1|0|0x02|0xFD|
|0|0|0|0| |0|0|1|1|0x03|0xFC|
|0|0|0|0| |0|1|0|0|0x04|0xFB|
|0|0|0|0| |0|1|0|1|0x05|0xFA|
|0|0|0|0| |0|1|1|0|0x06|0xF9|
|0|0|0|0| |0|1|1|1|0x07|0xF8|
|0|0|0|0| |1|0|0|0|0x08|0xF7|
|0|0|0|0| |1|0|0|1|0x09|0xF6|
|0|0|0|0| |1|0|1|0|0x0A|0xF5|
|0|0|0|0| |1|0|1|1|0x0B|0xF4|
|0|0|0|0| |1|0|0|0|0x0C|0xF3|
|0|0|0|0| |1|0|1|1|0x0D|0xF2|
|0|0|0|0| |1|0|1|0|0x0E|0xF1|
|0|0|0|0| |1|0|0|1|0x0F|0xF0|

e.g. If you want pin 7 and 2 on then
pin 2 from the first table gives 0x.D
pin 7 (=3 from table) gives 0x.B
So the result is 0xBD

|8|7|6|5| |4|3|2|1|data|data to send|
|-|-|-|-|-|-|-|-|-|----|-------------|
|0|1|0|0| |0|0|1|0|0x42|0xBD|

To set pin 7 and 2 on a board with address 0x21 use the following setting and press SET. Now you will see LED 2 and 7 of the connected relay board turned on. 

<img src="../static/example_i2c.png" width="300"/>

 
 
# support
If you have any questions or remarks please feel free to contact the author on Gitlab or the SIP [forum](https://nosack.com/sipforum/index.php).