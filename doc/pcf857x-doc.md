# SIP PCF857x IO Extender plugin
this plugin drives i2c IO extender boards to control the relay boards or valves. 
Up to 8 pcf8574 boards can be connected, so 64 stations can be used. Future versions of this plugin will, if sufficient demand, support also the 16 channel pcf8575 boards which can drive 128 stations. 

<img src="../static/i2c_8p_module.jpg" width="300"/>


## Requirements
The plugin is developed in Python3. Therefore a working SIP installation running under Python3 is required. Python2 is no longer supported. 
This plugin is supported and tested on the Raspberry Pi 3 and 4. All pcf8574 boards should work. The above example can be daisy chained so little to no soldering is required. 


__Caution: the 3.3v vs. 5v issue.__\
There are several ways to power the IO extender(s) and the relay boards. All options have some pros and cons. Read more on this [here](level_issue.md)

## Wiring
Next to wiring the i2c wires to the Pi the relay boards also need to be connected. This is straight forward, connect the VCC from the IO extender board to the VCC of the relay board. GND is not needed. The datalines can be connected 1-to-1 between the boards. Breadboard wires could be used but a more solid connector is preferable.

<img src="../static/pcf2relay.png" width="300"/>

__Important for safety:__ When choosing a relay board always use the ones with optocouplers and supply the 5v relays from a separate psu (JD-VCC jumper removed). In this way the relays and higher voltages (110 or 220v) will be galvanic separated from the Pi and connected (extender) modules.   


## Setup
### Enable i2c on the Pi
If not already done, the i2c interface on the Pi should be enabled. The easiest is via the command line by typing ```sudo raspi-config nonint do_i2c 0```  (Note the `0` at the end, this enables the i2c)

If you prefer the raspiconfig gui, a detailed description see [Adafuit configure i2c](https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c) for more details

### install the smbus library
Some SIP i2c plugins already have the library installed with it. If so, the same library is also used for this plugin. If there is a missing library message visible in the plugin page then the library has to be installed manually by issuing the following commands:\
```sudo apt-get install -y python3-smbus i2c-tools```
### SIP settings
First in SIP goto *Options→Station handling*. Add the number of extra boards above the default first one you need. So if you need a total of 24 stations you set Station extensions to: *2*  
Since almost all relay boards are active low, the *Active low relay* setting has to be checked.\
Submit the changes and go to the 857x plugin settings page.

<img src="../static/station_handling.png" width="200"/>

## Plugin settings


### Board jumper settings
Give every board a unique address. It does not matter which address you use as long there are no boards with the same address. Use the table or pictures below if needed.

### 0=jumper **right** &nbsp;&nbsp;&nbsp; 1=jumper **left**
|A0|A1|A2|Address|
|:--:|:--:|:--:|:------:
|0|0|0|0x20|
|1|0|0|0x21|
|0|1|0|0x22|
|1|1|0|0x23|
|0|0|1|0x24|
|1|0|1|0x25|
|0|1|1|0x26|
|1|1|1|0x27|


<img src="../static/8_module_addresses.png" width="100%"/>

&nbsp;
### Search for devices
To see which devices are connected use scan button. The plugin will search all i2c addresses and returns a list with devices found. 

**Note:** All I2C devices are detected. The pcf8574 IO extender boards usually have addresses from 0x20 to 0x27. Other (non IO) modules like lcd displays can also use this address space. So be sure to configure the boards correctly.  

**Note2:** If not all devices are detected there are most likely strong (low value) pull-up resistors on the boards. In that case use a small wire cutter and cut the the two resitors (next to the pin connector) in half. Do this on a few but not all boards.


Under settings enter for each board the right addres.  
<img src="../static/use_scan_results.png" width="100%"/>

Once the board table is filled press Submit to save the settings. SIP wil restart to use these new settings. 

### Testing
You can test the new boards by turning stations on or off.
Testing can also be done from the plugin page but a little knowledge of the hexadecimal system is needed. See this [page](advanced_i2c.md) for more on this.

# support
If you have any questions or remarks please feel free to contact the author on Gitlab or the SIP [forum](https://nosack.com/sipforum/index.php).