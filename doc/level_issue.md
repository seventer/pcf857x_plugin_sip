# The 3.3v vs. 5v issue.

The Raspberry Pi is powered from a 5v supply but the Pi itself operates at 3.3v. The IO extenders can operate at both 3.3v and 5v. The connected relay boards operate the best at 5v but most bords do work fine at 3.3V. So which power level do we choose?

We know (or assume) that:
* 5v on the Pi's IO lines will definitely kill the Pi 
* The Pi can supply very limited 3.3v power
* The Pi can usually supply some decent extra 5v power
* we need a solid/robust and stable setup

 
## Possible options in reverse order of preference

### Use 3.3v from the Pi
For a single board this will work but there isn't enough power to drive a full configuration. Also, you connect the modules directly to the Pi's power rail which is not the best practice to do.

### Use 5v from the Pi
Since the IO extender boards only pulls the data line down (connecting it to gnd) while communicating it is possible to connect the 5v module to the Pi. The Pi and IO boards deliver enough power to drive all the datalines from the maximum of 64 channles at te same time. But be aware that when a hardware failure on the IO board occurs you might (will) fry the Pi. 

### Use 3.3v from a separate powersupply (or Pi's 5v)
With a 3.3v regulator powered from the 5v of the Pi or other supply you could power the the full config of IO and relay modules. This is safe to do but you have to solder a regulator (e.g. [AMS7111-3.3](http://www.advanced-monolithic.com/pdf/ds1117.pdf)) in between the 5v and the vcc of the connected boards or use a little ($0.5) board which you can directly wire up.

<img src="../static/3v3_regulator.png" width="20%"/>  

## Use a logic level shifter (preferred option)
To connect the Pi safe to 5v devices a level-shifter is required. One board with at least two channels required. These level-shifter boards will cost around 1$ in most Asian webshops. 

**It is highly recommended that a level-shifter is installed!**


Read [this](https://cdn-shop.adafruit.com/datasheets/AN10441.pdf) if you want to know more about the level shifter.


### Example setup
The following is (just) an example of a level-shifter inbetween the Pi and the IO extender boards. This setup can drive a full config of 8 io extenders and 8x8-channel relay boards (with JD-VCC jumper removed).  

<img src="../static/level_shifter_setup.png" width="100%"/>

# support
If you have any questions or remarks please feel free to contact the author on Gitlab or the SIP [forum](https://nosack.com/sipforum/index.php).
